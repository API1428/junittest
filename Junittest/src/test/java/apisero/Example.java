package apisero;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class Example {
	static oddeven obj;
	@BeforeAll
	public static void abc1()
	 {
		obj = new oddeven();
		 System.out.println("I am BeforeAll");
	 }
	
	 @BeforeEach
	 public void abc2() {
		 
		 System.out.println("I am BeforeEach");
	 }
	 @AfterAll
	 public  static  void abc3()
	 {
		 System.out.println("I am AfterAll");
	 }
	  
	 
	@AfterEach
	 public void abc4()
	 {
		 System.out.println("I am AfterEach");
	 }
	 @Test
	 public void abc5()
	 {
		 
		 assertNotNull(obj.evenodd(56));
		 System.out.println("I am TestCase");
	 }
	 @Test
	 public void abc6() 
	 {
		 System.out.println("I am TestCase");
	 }
	 
}
